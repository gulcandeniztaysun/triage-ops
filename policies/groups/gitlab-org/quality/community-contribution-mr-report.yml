resource_rules:
  merge_requests:
    summaries:
      - name: Identify idle and recently merged ~"Community contribution" merge requests
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Community contributions report
            summary: |
              Assignees should go through both lists and process the merge requests in them. All merge requests require a response. After processing a merge request, the checkbox on the left should be checked.

              When going through the list of merge requests, please do one of the following:

              - If a reviewer is assigned and the last comment is from the reviewer, ping the author in a comment and set ~"workflow::in dev".
              - If a reviewer is assigned and the last comment is from the author, ping the reviewer in a comment, and set ~"workflow::ready for review".
              - If no reviewer is assigned, use https://gitlab-org.gitlab.io/gitlab-roulette/ or https://about.gitlab.com/handbook/product/product-categories/ to find a reviewer and assign the merge request to them.
              - If the merge request depends on the completion of work elsewhere, set ~"workflow::blocked".

              Overview of merge requests:

              - In dev (author should get back to us) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%20dev
              - Ready for review (a team member should pick the review) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Aready%20for%20review
              - In review (a team member is actively reviewing) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ain%20review
              - Blocked (inactionable) - https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community%20contribution&label_name[]=workflow%3A%3Ablocked

              For more info about the process, see https://about.gitlab.com/handbook/engineering/quality/contributor-success/#working-with-community-contributions.

              #{
              group = Hierarchy::Group.new("contributor_success")
              short_team_summary(group_key: "contributor_success", title: "## Community Contribution Report", extra_assignees: group.mentions + ["@zillemarco", "@taucher2003"])
              }

              ----

              Job URL: #{ENV['CI_JOB_URL']}

              *This report was generated from [this policy](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/#{ENV['TRIAGE_POLICY_FILE']}).*
        rules:
          - name: 1. First-time contributor merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - 1st contribution
              forbidden_labels:
                - idle
                - stale
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
                - "group::runner"
                - "group::hosted runners"
                - "group::distribution"
              ruby: |
                IdleMrHelper.idle?(resource, days: 7)
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|devops::|group::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"1st contribution" merge requests awaiting GitLab response (7 days without human interaction)

                  {{items}}
          - name: 2. Idle ~"Community Contribution" merge requests awaiting GitLab response
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
              forbidden_labels:
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
                - "group::runner"
                - "group::hosted runners"
                - "group::distribution"
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|devops::|group::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} Idle (21 days without human interaction) merge requests awaiting GitLab response

                  {{items}}
          - name: 3. Distribution merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - "group::distribution"
              forbidden_labels:
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
              ruby: |
                resource[:labels].include?('idle') || (resource[:labels].include?('1st contribution') && IdleMrHelper.idle?(resource, days: 7))
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"group::distribution" Idle merge requests awaiting GitLab response

                  ~"group::distribution" has a [different review workflow](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/merge_requests.html#workflow).
                  **Don't change any workflow labels while working through this list.**
                  Just make sure that no merge requests are stuck and waiting for review for too long.

                  If merge requests appear to be stuck, consider pinging the assigned reviewer or `@gitlab-org/distribution` if no reviewer is assigned.

                  {{items}}
          - name: 4. Runner merge requests
            conditions:
              state: opened
              labels:
                - Community contribution
                - idle
              forbidden_labels:
                - "automation:bot-authored"
                - "workflow::in dev" # author is working on the issue
                - "workflow::blocked"
              ruby: (resource[:labels] & ['group::runner', 'group::hosted runners']).any?
            actions:
              summarize:
                item: |
                  - [ ] idle: #{IdleMrHelper.days_since_last_human_update(resource)}d | `{{author}}` | #{resource[:web_url]}+ | #{resource[:labels].grep(/^(type::|workflow::|Leading Organization)/).map { |label| %(~"#{label}") }.join(' ')}
                summary: |
                  ### #{resource[:items].lines.size} ~"group::runner" & ~"group::hosted runners" Idle (21 days without human interaction) merge requests awaiting GitLab response

                  {{items}}
          - name: 5. Merged ~"Community contribution" merge requests
            api: graphql
            conditions:
              state: merged
              labels:
                - Community contribution
              forbidden_labels:
                - "automation:bot-authored"
              date:
                attribute: merged_at
                condition: newer_than
                interval_type: days
                interval: 7
            actions:
              summarize:
                item: |
                  `{{author}}`,#{resource[:web_url]},{{labels}}
                summary: |
                  #{ mrs = merge_requests_from_line_items(resource[:items]); nil }
                  #{ author_count = mrs.group_by { |mr| mr[:author] }.count; nil }

                  **#{mrs.count}** ~"Community contribution" merge requests from **#{author_count} unique authors** merged in the past 7 days
