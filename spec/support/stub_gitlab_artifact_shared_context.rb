# frozen_string_literal: true

# rubocop:disable RSpec/InstanceVariable
# This context expects:
# - artifacts: a { artifact_pah => artifact_content } hash
# - project_id
# - job_id
RSpec.shared_context 'with stubbed GitLab artifact' do
  before do
    @artifact_files ||= []

    artifacts.each do |artifact_path, artifact_content|
      artifact_file = Gitlab::FileResponse.new(Tempfile.new(File.basename(artifact_path)))
      @artifact_files << artifact_file

      allow(Triage.api_client)
        .to receive(:download_job_artifact_file)
        .with(project_id, job_id, artifact_path)
        .and_return(artifact_file)

      artifact_file.write(artifact_content)
      artifact_file.rewind
    end
  end

  after do
    @artifact_files.map(&:unlink)
  end
end
# rubocop:enable RSpec/InstanceVariable
