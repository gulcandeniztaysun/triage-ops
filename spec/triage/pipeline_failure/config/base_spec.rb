# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/base'

RSpec.describe Triage::PipelineFailure::Config::Base do
  let(:event) { instance_double(Triage::PipelineEvent) }
  let(:config_class) { Class.new(described_class) }

  subject { config_class.new(event) }

  describe '.match?' do
    context 'when config class does not implement .match?' do
      it 'raises NotImplementedError' do
        expect { described_class.match?(event) }
          .to raise_error(NotImplementedError)
      end
    end

    context 'when config class implements .match?' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def self.match?(event)
            true
          end
        end
      end

      it 'returns true' do
        expect(config_class.match?(event)).to be(true)
      end
    end
  end

  describe '#create_incident?' do
    shared_examples 'falsy response' do
      it 'returns false' do
        expect(subject.create_incident?).to be_falsy
      end
    end

    it_behaves_like 'falsy response'

    context 'when config class implements #incident_project_id only' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_project_id
            1234
          end
        end

        it_behaves_like 'falsy response'
      end
    end

    context 'when config class implements #incident_template only' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_template
            'foo'
          end
        end

        it_behaves_like 'falsy response'
      end
    end

    context 'when config class implements both #incident_template & #incident_template' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_project_id
            1234
          end

          def incident_template
            'foo'
          end
        end

        it 'returns true' do
          expect(subject.create_incident?).to be_truthy
        end
      end
    end
  end

  describe '#incident_project_id' do
    context 'when config class does not implement #incident_project_id' do
      it 'returns nil' do
        expect(subject.incident_project_id).to be_nil
      end
    end

    context 'when config class implements #incident_project_id' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_project_id
            1234
          end
        end
      end

      it 'returns 1234' do
        expect(subject.incident_project_id).to eq(1234)
      end
    end
  end

  describe '#incident_template' do
    context 'when config class does not implement #incident_template' do
      it 'returns nil' do
        expect(subject.incident_project_id).to be_nil
      end
    end

    context 'when config class implements #incident_template' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_template
            'foo'
          end
        end
      end

      it 'returns "foo"' do
        expect(subject.incident_template).to eq('foo')
      end
    end
  end

  describe '#incident_extra_attrs' do
    context 'when config class does not implement #incident_extra_attrs' do
      it 'returns default implementation' do
        expect(subject.incident_extra_attrs).to eq({})
      end
    end

    context 'when config class implements #incident_extra_attrs' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_extra_attrs
            { hello: 'world' }
          end
        end
      end

      it 'returns { hello: "world" }' do
        expect(subject.incident_extra_attrs).to eq({ hello: 'world' })
      end
    end
  end

  describe '#incident_labels' do
    context 'when config class does not implement #incident_labels' do
      it 'returns default implementation' do
        expect(subject.incident_labels).to eq([])
      end
    end

    context 'when config class implements #incident_labels' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def incident_labels
            %w[hello world]
          end
        end
      end

      it 'returns %w[hello world]' do
        expect(subject.incident_labels).to eq(%w[hello world])
      end
    end
  end

  describe '#default_slack_channels' do
    context 'when config class does not implement #default_slack_channels' do
      it 'returns empty array' do
        expect(subject.default_slack_channels).to eq([])
      end
    end

    context 'when config class implements #default_slack_channels' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def default_slack_channels
            ['foo']
          end
        end
      end

      it 'returns "foo"' do
        expect(subject.default_slack_channels).to eq(['foo'])
      end
    end
  end

  describe '#slack_username' do
    context 'when config class does not implement #slack_channel' do
      it 'returns the default username' do
        expect(subject.slack_username).to eq(described_class::DEFAULT_SLACK_USERNAME)
      end
    end

    context 'when config class implements #slack_username' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def slack_username
            'foo'
          end
        end
      end

      it 'returns "foo"' do
        expect(subject.slack_username).to eq('foo')
      end
    end
  end

  describe '#slack_icon' do
    context 'when config class does not implement #slack_channel' do
      it 'returns the default username' do
        expect(subject.slack_icon).to eq(described_class::DEFAULT_SLACK_ICON)
      end
    end

    context 'when config class implements #slack_icon' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def slack_icon
            'foo'
          end
        end
      end

      it 'returns "foo"' do
        expect(subject.slack_icon).to eq('foo')
      end
    end
  end

  describe '#slack_payload_template' do
    context 'when config class does not implement #slack_payload_template' do
      it 'returns the default template' do
        expect(subject.slack_payload_template).to eq(described_class::DEFAULT_SLACK_PAYLOAD_TEMPLATE)
      end
    end

    context 'when config class implements #slack_payload_template' do
      let(:config_class) do
        Class.new(Triage::PipelineFailure::Config::Base) do
          def slack_payload_template
            'foo'
          end
        end
      end

      it 'returns "foo"' do
        expect(subject.slack_payload_template).to eq('foo')
      end
    end
  end
end
