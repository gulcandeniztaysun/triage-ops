# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/processor'
require_relative '../../triage/triage/percentage_rollout'

RSpec.describe Triage::PercentageRollout do
  let(:project_id) { 123 }
  let(:iid) { 456 }
  let(:sample_size) { 1000 }
  let(:events) do
    (1..sample_size).map do |i|
      stubbed_event(iid: i, event_class: Triage::Event)
    end
  end

  let!(:counter_class) do
    Class.new do
      attr_reader :count

      def initialize
        @count = 0
      end

      def add
        @count += 1
      end
    end
  end

  context 'with processors having percentage roll out' do
    let(:processor_class_30_percent) do
      Class.new(Triage::Processor) do
        react_to 'issue.*'
        percentage_rollout threshold: 30, on: ->(event) { event.iid.to_s }

        def initialize(event, counter)
          super(event)
          @counter = counter
        end

        def process
          @counter.add
        end

        def applicable?
          rollout.rolled_out?(event)
        end
      end
    end

    let(:processor_class_50_percent) do
      Class.new(Triage::Processor) do
        react_to 'issue.*'
        percentage_rollout threshold: 50, on: ->(event) { event.iid.to_s }

        def initialize(event, counter)
          super(event)
          @counter = counter
        end

        def process
          @counter.add
        end

        def applicable?
          rollout.rolled_out?(event)
        end
      end
    end

    let(:counter_30) { counter_class.new }
    let(:counter_50) { counter_class.new }

    include_context 'with event'

    it 'processes the given percentages of events' do
      events.each do |event|
        processor_30 = processor_class_30_percent.new(event, counter_30)
        processor_50 = processor_class_50_percent.new(event, counter_50)

        processor_30.triage
        processor_50.triage
      end

      expect(counter_30.count).to be_within(10).of(300)
      expect(counter_50.count).to be_within(10).of(500)
    end
  end

  context 'with processor that specifies a negative percentage roll out' do
    let(:processor_class) do
      Class.new(Triage::Processor) do
        react_to 'issue.*'
        percentage_rollout threshold: -10, on: ->(event) { event.iid.to_s }

        def applicable?
          rollout.rolled_out?(event)
        end
      end
    end

    let(:processor) { processor_class.new(event) }

    include_context 'with event'

    it 'does not call the hooks' do
      expect(processor).not_to receive(:before_process)
      expect(processor).not_to receive(:process)
      expect(processor).not_to receive(:after_process)

      processor.triage
    end
  end

  context 'with processor which assumes rolling out on a specific event but it is not the event' do
    let(:processor_class) do
      Class.new(Triage::Processor) do
        react_to 'issue.*'
        percentage_rollout threshold: 25, on: ->(event) { event.iid.to_s }

        def applicable?
          event.merge_request? && rollout.rolled_out?(event)
        end
      end
    end

    let(:processor) { processor_class.new(event) }

    include_context 'with event' do
      let(:event_attrs) do
        {
          merge_request?: false
        }
      end
    end

    it 'does not call the hooks' do
      expect(processor).not_to receive(:before_process)
      expect(processor).not_to receive(:process)
      expect(processor).not_to receive(:after_process)

      processor.triage
    end
  end
end
