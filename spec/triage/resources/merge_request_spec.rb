# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage'
require_relative '../../../triage/resources/merge_request'

RSpec.describe Triage::MergeRequest do
  let(:project_id) { 222 }
  let(:iid) { 123 }
  let(:title) { 'MR title' }
  let(:web_url) { 'web_url' }
  let(:labels) { %w[foo bar] }
  let(:author) { { 'id' => 42 } }
  let(:mr_attrs) do
    {
      'iid' => iid,
      'title' => title,
      'web_url' => web_url,
      'labels' => labels,
      'author' => author
    }
  end

  subject { described_class.new(mr_attrs) }

  describe '.build' do
    context 'when given attributes are nil' do
      it 'returns nil' do
        expect(described_class.build(nil)).to be_nil
      end
    end

    context 'when given attributes are not nil' do
      it 'returns a MergeRequest object' do
        expect(described_class.build(author: 'John Doe')).to be_a(described_class)
      end
    end
  end

  describe '.find' do
    it 'makes a request to the relevant endpoint' do
      expect_api_request(path: "/projects/#{project_id}/merge_requests/#{iid}", response_body: mr_attrs) do
        described_class.find(project_id, iid)
      end
    end
  end

  describe '#iid' do
    it 'returns the MR iid' do
      expect(subject.iid).to eq(iid)
    end
  end

  describe '#title' do
    it 'returns the MR title' do
      expect(subject.title).to eq(title)
    end
  end

  describe '#web_url' do
    it 'returns the MR web_url' do
      expect(subject.web_url).to eq(web_url)
    end

    context 'when attributes has a url key' do
      context 'without web_url key' do
        it 'returns the url value' do
          expect(described_class.build(url: 'url').web_url).to eq('url')
        end
      end

      context 'with web_url key' do
        it 'returns the web_url value' do
          expect(described_class.build(url: 'url', web_url: 'web_url').web_url).to eq('web_url')
        end
      end
    end
  end

  describe '#labels' do
    it 'returns the MR labels' do
      expect(subject.labels).to eq(labels)
    end
  end

  describe '#author' do
    it 'returns the MR author' do
      expect(subject.author).to eq(author)
    end
  end
end
