# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/hierarchy/stage'

RSpec.describe Hierarchy::Stage do
  subject(:stage) { described_class.new('stage_with_two_groups') }

  describe '.raw_data' do
    it 'returns WwwGitLabCom.groups data' do
      expect(described_class.raw_data).to include('stage_with_two_groups' => hash_including('name' => 'Stage with two groups'))
    end
  end

  describe '#groups' do
    it 'returns a hash' do
      expect(stage.groups.first.name).to eq('Group 1')
    end
  end

  describe '#to_h' do
    it 'returns a hash' do
      expect(stage.to_h.keys).to eq(%i[assignees labels mentions groups])
    end
  end
end
