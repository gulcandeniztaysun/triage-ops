# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/www_gitlab_com'

RSpec.describe WwwGitLabCom do
  describe '.sections' do
    it 'fetches sections from www-gitlab-com data' do
      expect(described_class.sections.keys).to eq(%w[section1 section2])
    end
  end

  describe '.stages' do
    it 'fetches stages from www-gitlab-com data' do
      expect(described_class.stages.keys).to include('stage_with_two_groups', 'stage_with_one_group')
    end
  end

  describe '.groups' do
    it 'fetches groups from www-gitlab-com' do
      expect(described_class.groups.keys).to include('group1', 'group2', 'group3')
    end
  end

  describe '.categories' do
    it 'fetches categories from www-gitlab-com' do
      expect(described_class.categories.keys).to include('category1', 'category2', 'category3', 'category4', 'category5', 'category_with_no_group')
    end
  end

  describe '.team_from_www' do
    it 'fetches team from www-gitlab-com' do
      expect(described_class.team_from_www['p-1'].keys).to include('departments')
    end
  end

  describe '.roulette' do
    it 'fetches roulette data' do
      expect(described_class.roulette[0].keys).to include('username')
    end
  end

  describe '.distribution_projects' do
    it 'fetches distribution projects' do
      expect(described_class.distribution_projects).to include(4359271) # https://gitlab.com/gitlab-org/build/cng
    end
  end
end
