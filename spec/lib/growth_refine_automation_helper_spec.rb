# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/constants/labels'
require_relative '../../lib/growth_refine_automation_helper'

RSpec.describe GrowthRefineAutomationHelper do
  let(:issue_resource) do
    Struct.new(:resource) do
      include GrowthRefineAutomationHelper
    end
  end

  describe '#growth_refinement_completed?' do
    let(:project_id) { 1234 }
    let(:iid) { 5678 }

    let(:resource) do
      { project_id: project_id, iid: iid, type: 'issues' }
    end

    subject { issue_resource.new(resource) }

    context 'when no refinement_thread is found in the issue' do
      let(:empty_comments) { [Gitlab::ObjectifiedHash.new({ 'body' => 'empty' })] }

      it 'returns false' do
        allow(Triage.api_client).to receive(:issue_notes).with(project_id, iid).and_return(Gitlab::PaginatedResponse.new(empty_comments))
        expect(subject.growth_refinement_completed?).to be false
      end
    end

    context 'when refinement_thread is found' do
      let(:contain_refine_comments) do
        [
          Gitlab::ObjectifiedHash.new({
            'body' => '## :construction: Refinement',
            'id' => 99
          })
        ]
      end

      let(:react_emoji) { 'one' }

      let(:valid_reactions) do
        [
          {
            'name' => react_emoji,
            'user' => { 'id' => 1 }
          },
          {
            'name' => 'two',
            'user' => { 'id' => 11 }
          },
          {
            'name' => 'three',
            'user' => { 'id' => 111 }
          }
        ].map { |reaction| Gitlab::ObjectifiedHash.new(reaction) }
      end

      before do
        allow(Triage.api_client).to receive(:issue_notes).with(project_id, iid).and_return(Gitlab::PaginatedResponse.new(contain_refine_comments))
      end

      context 'without any emoji reaction' do
        it 'returns false' do
          allow(Triage.api_client).to receive(:note_award_emojis).and_return([])
          expect(subject.growth_refinement_completed?).to be false
        end
      end

      context 'with enough unique valid reactions' do
        before do
          allow(Triage.api_client).to receive(:note_award_emojis).with(project_id, iid, 'issue', 99).and_return(valid_reactions)
        end

        context 'when dry run' do
          before do
            allow(Triage).to receive(:dry_run?).and_return true
          end

          it 'returns true and does not trigger adding checkmark and updating weight with max weight' do
            expect(Triage.api_client).not_to receive(:create_note_award_emoji)
            expect(Triage.api_client).not_to receive(:edit_issue)
            expect(subject.growth_refinement_completed?).to be true
          end
        end

        context 'when not dry run' do
          before do
            allow(Triage).to receive(:dry_run?).and_return false
          end

          it 'returns true and triggers adding checkmark and updating weight with max weight' do
            expect(Triage.api_client).to receive(:create_note_award_emoji).with(project_id, iid, 'issue', 99, 'white_check_mark')
            expect(Triage.api_client).to receive(:edit_issue).with(project_id, iid, weight: 3)
            expect(subject.growth_refinement_completed?).to be true
          end
        end

        context "with reactions containing 'x'" do
          let(:react_emoji) { 'x' }

          it 'returns false to block refinement' do
            expect(subject.growth_refinement_completed?).to be false
          end
        end
      end
    end
  end

  describe '#qualify_for_refine?' do
    let(:growth_labels) { Labels::GROWTH_TEAM_LABELS.join(',') }
    let(:refine_list_labels) { "#{Labels::WORKFLOW_REFINEMENT_LABEL},#{growth_labels}" }
    let(:plan_list_labels) { "#{Labels::WORKFLOW_PLANNING_BREAKDOWN_LABEL},#{growth_labels}" }
    let(:gitlab_org_group_id) { 9970 }
    let(:growth_kanban_board_id) { 4152639 }

    let(:board_lists) { Gitlab::ObjectifiedHash.new({ 'lists' => [{ 'max_issue_count' => 2, 'label' => { 'name' => Labels::WORKFLOW_REFINEMENT_LABEL } }] }) }
    let(:refine_issues) { [] }
    let(:qualified_issues) { [{ 'iid' => 1 }, { 'iid' => 2 }].map { |issue| Gitlab::ObjectifiedHash.new(issue) } }

    before do
      allow(Triage.api_client).to receive(:group_board).with(gitlab_org_group_id, growth_kanban_board_id).and_return(board_lists)
      allow(Triage.api_client).to receive(:group_issues).with(gitlab_org_group_id, labels: refine_list_labels, state: 'opened').and_return(refine_issues)
      allow(Triage.api_client).to receive(:group_issues).with(gitlab_org_group_id, labels: plan_list_labels, state: 'opened', order_by: 'relative_position', sort: 'asc', per_page: 2).and_return(qualified_issues)
    end

    context 'when the issue is qualified' do
      subject { issue_resource.new({ iid: 1 }) }

      it 'returns true' do
        expect(subject.qualify_for_growth_refine?).to be true
      end
    end

    context 'when the issue is not qualified' do
      subject { issue_resource.new({ iid: 3 }) }

      it 'returns false' do
        expect(subject.qualify_for_growth_refine?).to be false
      end
    end
  end
end
