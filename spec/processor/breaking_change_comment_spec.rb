# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/breaking_change_comment'
require_relative '../../triage/triage/event'

RSpec.describe Triage::BreakingChangeComment do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'update',
        from_gitlab_org?: true,
        added_label_names: added_label_names,
        project_id: project_id,
        iid: merge_request_iid
      }
    end

    let(:project_id) { 123 }
    let(:merge_request_iid) { 300 }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.open']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when there is no `breaking change` label' do
      let(:added_label_names) { ['somelabel'] }

      include_examples 'event is not applicable'
    end

    context 'when there is the `breaking change` label' do
      before do
        stub_api_request(
          path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
          query: { per_page: 100 },
          response_body: [])
      end

      let(:added_label_names) { ['breaking change'] }

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:label) { 'breaking change' }
    let(:added_label_names) { [label] }

    it 'posts a deprecation message' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          @root thanks for adding the ~"breaking change" label!

          This merge request introduces breaking changes. [Learn more about breaking changes](https://docs.gitlab.com/ee/development/deprecation_guidelines/).

          It's important to identify how the breaking change was introduced. To estimate the impact, try to assess the following:

          - Are there existing users depending on this feature?
            - Are self-managed customers affected?
            - To verify and quantify usage, use Grafana or Kibana.
            - If you're not sure about how to query the data, contact the infrastructure team on their Slack channel, #infrastructure-lounge
          - Was sufficient time given to communicate the change?
          - Changes in the permissions, the API schema, and the API response might affect existing 3rd party integrations.
            - Reach out to the Support team or Technical Account Managers and ask about the possible impact of this change.
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
