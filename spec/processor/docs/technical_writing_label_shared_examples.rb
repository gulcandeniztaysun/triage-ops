# frozen_string_literal: true

require_relative '../../../triage/triage/event'

RSpec.shared_examples 'technical writing label features' do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:action) { nil }
    let(:technical_writing_label_present) { nil }
    let(:from_gitlab_org) { nil }
    let(:event_actor_id) { nil }
    let(:team_member_id_docs_team) { 1 }
    let(:team_member_id_other_team) { 2 }
    let(:gitlab_docs_team_member_ids) { [team_member_id_docs_team] }

    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: action,
        from_gitlab_org?: from_gitlab_org,
        event_actor_id: event_actor_id,
        label_names: technical_writing_label_present ? ['Technical Writing'] : []
      }
    end
  end

  subject { described_class.new(event) }

  before do
    allow(Triage).to receive(:gitlab_docs_team_member_ids).and_return(gitlab_docs_team_member_ids)
  end

  describe '#applicable?' do
    context 'when event is not from gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event is from gitlab-org' do
      let(:from_gitlab_org) { true }

      context "when the 'Technical Writing' label has already been applied" do
        let(:technical_writing_label_present) { true }

        include_examples 'event is not applicable'
      end

      context "when the 'Technical Writing' label hasn't been applied" do
        let(:technical_writing_label_present) { false }

        context 'with an approver that is not in the docs team' do
          let(:event_actor_id) { team_member_id_other_team }

          include_examples 'event is not applicable'
        end

        context 'with an approver that is in the docs team' do
          let(:event_actor_id) { team_member_id_docs_team }
          let(:no_previous_unique_comment) { nil }

          before do
            allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(no_previous_unique_comment)
          end

          context 'when the unique comment already exists' do
            let(:no_previous_unique_comment) { false }

            include_examples 'event is not applicable'
          end

          context "when the unique comment doesn't exist" do
            let(:no_previous_unique_comment) { true }

            include_examples 'event is applicable'
          end
        end
      end
    end
  end

  describe '#process' do
    it 'adds a unique comment that includes a quick action to apply the label' do
      body = <<~MARKDOWN.chomp
        #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
        Hi `@root` :wave:,

        GitLab Bot has added the ~"Technical Writing" label because a Technical Writer has
        [approved or merged](https://about.gitlab.com/handbook/product/ux/technical-writing/#review-workflow)
        this MR.

        /label ~"Technical Writing"

        *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
        You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/processor/docs/technical_writing_label_base.rb).*
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
