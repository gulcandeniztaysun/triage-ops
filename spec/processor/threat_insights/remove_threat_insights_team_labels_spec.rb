# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../lib/constants/labels'
require_relative '../../../triage/processor/threat_insights/remove_threat_insights_team_labels'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::RemoveThreatInsightsTeamLabels do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        from_part_of_product_project?: true,
        removed_label_names: [Labels::THREAT_INSIGHTS_GROUP_LABEL]
      }
    end

    let(:label_names) { [Labels::THREAT_INSIGHTS_TEAM_LABELS[0]] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not a part of product' do
      before do
        allow(event).to receive(:from_part_of_product_project?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context "when ~\"#{Labels::THREAT_INSIGHTS_GROUP_LABEL}\" is not removed" do
      before do
        allow(event).to receive(:removed_label_names).and_return([])
      end

      include_examples 'event is not applicable'
    end

    context 'when project is a part of product' do
      context "when ~\"#{Labels::THREAT_INSIGHTS_GROUP_LABEL}\" is removed" do
        context 'when threat insights team label is not present' do
          let(:label_names) { [] }

          include_examples 'event is not applicable'
        end

        context 'when threat insights team label is present' do
          Labels::THREAT_INSIGHTS_TEAM_LABELS.each do |team_label|
            context "with team label #{team_label}" do
              let(:label_names) { [team_label] }

              include_examples 'event is applicable'
            end
          end
        end
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = "/unlabel #{Labels::THREAT_INSIGHTS_TEAM_LABELS.map { |l| %(~"#{l}") }.join(' ')}"

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
