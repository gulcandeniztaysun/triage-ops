# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/constants/labels'
require_relative '../../triage/processor/assign_dev_for_verification'
require_relative '../../triage/triage/event'

RSpec.describe Triage::AssignDevForVerification do
  include_context 'with event', Triage::IssueEvent do
    let(:label_names) { ['workflow::verification', 'group::security policies', 'backend'] }
    let(:project_id) { 12 }
    let(:issue_iid) { 1234 }
    let(:event_attrs) do
      {
        project_id: project_id,
        iid: issue_iid,
        from_gitlab_org?: true,
        assignee_ids: [],
        label_names: label_names
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event project is not a part of gitlab org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when group and workflow labels are not in label_names' do
      before do
        allow(event).to receive(:label_names).and_return([])
      end

      include_examples 'event is not applicable'
    end

    context 'when issue is assigned' do
      before do
        allow(event).to receive(:assignee_ids).and_return([1])
      end

      include_examples 'event is not applicable'
    end

    context 'when labels are not valid' do
      before do
        allow(event).to receive(:label_names).and_return(["workflow::refinement"])
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:security_policies_be_members) { %w[@security_policies_be_1 @security_policies_be_2] }
    let(:threat_insights_be_members) { %w[@threat_insights_be_1 @threat_insights_be_2] }
    let(:threat_insights_fe_members) { %w[@threat_insights_fe_1 @threat_insights_fe_2] }

    before do
      allow(subject).to receive(:sleep)
    end

    shared_examples 'processes event' do |assigned_username, unassigned_username: nil|
      before do
        allow(subject).to receive(:security_policies_be).with(except: unassigned_username) do
          (security_policies_be_members - ["@#{unassigned_username}"]).first
        end

        allow(subject).to receive(:threat_insights_be).with(except: unassigned_username) do
          (threat_insights_be_members - ["@#{unassigned_username}"]).first
        end

        allow(subject).to receive(:threat_insights_fe).with(except: unassigned_username) do
          (threat_insights_fe_members - ["@#{unassigned_username}"]).first
        end
      end

      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          assignees: [],
          state: 'opened'
        }
      end

      let(:notes) do
        if unassigned_username
          [{ created_at: "2013-10-02T09:22:45Z",
             "body" => "unassigned @#{unassigned_username}" }]
        else
          []
        end
      end

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          This issue is ready to be verified and according to our [verification process](https://about.gitlab.com/handbook/engineering/development/sec/govern/sp-ti-planning.html#verification)
          we need your help with this activity.

          #{assigned_username}, would you mind taking a look if this issue can be verified on production and close this issue?

          /assign #{assigned_username}
        MARKDOWN

        expect_api_requests do |requests|
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue)
          requests << stub_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}/notes", response_body: notes)
          requests << stub_comment_request(event: event, body: body)

          subject.process
        end
      end
    end

    context 'when issue belongs to security policies' do
      let(:label_names) { ['workflow::verification', 'group::security policies', 'backend'] }

      it_behaves_like 'processes event', '@security_policies_be_1'
      it_behaves_like 'processes event', '@security_policies_be_2', unassigned_username: 'security_policies_be_1'
    end

    context 'when issue belongs to threat insights frontend' do
      let(:label_names) { ['workflow::verification', Labels::THREAT_INSIGHTS_GROUP_LABEL, 'frontend'] }

      it_behaves_like 'processes event', '@threat_insights_fe_1'
      it_behaves_like 'processes event', '@threat_insights_fe_2', unassigned_username: 'threat_insights_fe_1'
    end

    context 'when issue belongs to threat insights backend' do
      let(:label_names) { ['workflow::verification', Labels::THREAT_INSIGHTS_GROUP_LABEL, 'backend'] }

      it_behaves_like 'processes event', '@threat_insights_be_1'
      it_behaves_like 'processes event', '@threat_insights_be_2', unassigned_username: 'threat_insights_be_1'
    end

    context 'when group label is different' do
      let(:label_names) { ['workflow::verification', 'group::composition analysis', 'backend'] }

      it 'raises exception' do
        expect { subject.process }.to raise_error(Triage::AssignDevForVerification::InvalidLabelsError)
      end
    end

    context 'when there is assignee' do
      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          assignees: [{ id: 1 }],
          state: 'opened'
        }
      end

      it 'does not assign dev' do
        expect_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue) do
          subject.process
        end
      end
    end

    context 'when issue is closed' do
      let(:issue) do
        {
          project_id: project_id,
          iid: issue_iid,
          state: 'closed'
        }
      end

      it 'does not assign dev' do
        expect_api_request(path: "/projects/#{project_id}/issues/#{issue_iid}", response_body: issue) do
          subject.process
        end
      end
    end
  end
end
