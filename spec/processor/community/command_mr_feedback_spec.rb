# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_feedback'

RSpec.describe Triage::CommandMrFeedback do
  include_context 'with slack posting context'
  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:event_attrs) do
      {
        id: 500,
        new_comment: %(@gitlab-bot feedback #{comment}),
        url: 'https://comment.url/note#123',
        noteable_author: Triage::User.new(username: noteable_author),
        iid: 123,
        event_actor_username: event_actor_username
      }
    end

    let(:comment) { 'awesome feedback' }
    let(:noteable_author) { 'agarcia' }
    let(:event_actor_username) { 'jchang' }
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', 'feedback'
  include_examples 'processor slack options', '#mr-feedback'

  describe '#applicable?' do
    before do
      allow(subject).to receive(:post_reaction_on_feedback)
    end

    include_context 'when command is a valid command from a wider community contribution'

    it_behaves_like 'community contribution command processor #applicable?'
    it_behaves_like 'rate limited Slack message posting', count: 1, period: 86400
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      allow(subject).to receive(:post_reaction_on_feedback).once
    end

    it_behaves_like 'slack message posting' do
      let(:message_body) do
        {
          text: <<~MARKDOWN,
            Hola MR coaches, `@#{event_actor_username}` has left feedback about their experience in #{event.url}.
          MARKDOWN
          attachments: [{ text: comment }]
        }
      end
    end
  end

  describe '#post_reaction_on_feedback' do
    it 'posts a reaction on the given feedback' do
      req = stub_request(:post, 'https://gitlab.com/api/v4/projects/123/merge_requests/123/notes/500/award_emoji')
        .with(body: { "name" => "white_check_mark" })
        .to_return(status: 200)

      subject.process

      expect(req).to have_been_made.once
    end
  end
end
