# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/processor/workflow/infradev_issue_label_nudger'
require_relative '../../../triage/triage/event'
require_relative '../../../triage/resources/issue'

RSpec.describe Triage::Workflow::InfradevIssueLabelNudger do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      { project_id: project_id,
        event_actor_username: 'username',
        from_gitlab_org?: from_gitlab_org,
        new_entity?: new_entity,
        removed_label_names: removed_label_names,
        added_label_names: added_label_names,
        label_names: label_names,
        resource_open?: resource_open }
    end

    let(:from_gitlab_org)     { true }
    let(:resource_open)       { true }
    let(:new_entity)          { true }
    let(:removed_label_names) { [] }
    let(:added_label_names)   { [] }
    let(:label_names)         { ['infradev'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.open', 'issue.update', 'issue.reopen']

  describe '#applicable?' do
    context 'when event emitted from gitlab org open issue, and has only infradev label' do
      include_examples 'applicable on contextual event'

      context 'when group label was removed from infradev issue' do
        let(:new_entity)          { false }
        let(:removed_label_names) { ['group::group1'] }

        include_examples 'event is applicable'
      end

      context 'when infradev label was added without group label' do
        let(:new_entity)          { false }
        let(:added_label_names)   { ['infradev'] }

        include_examples 'event is applicable'
      end
    end

    context 'when event is not triggered by new issue and does not have any label change' do
      let(:new_entity)          { false }
      let(:removed_label_names) { [] }
      let(:added_label_names)   { [] }

      include_examples 'event is not applicable'
    end

    context 'when event is not from gitlab_org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when event resource has no infradev label' do
      let(:label_names) { [''] }

      include_examples 'event is not applicable'
    end

    context 'when event resource is closed' do
      let(:resource_open) { false }

      include_examples 'event is not applicable'
    end

    context 'when event has infradev label and group label' do
      let(:label_names) { ['infradev', 'group::group1'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'nudges for group label 5 mins later' do
      expect(Triage::InfradevIssueLabelNudgerJob).to receive(:perform_in).with(Triage::DEFAULT_ASYNC_DELAY_MINUTES, event)
      subject.process
    end
  end
end
