stages:
  - dry-run
  - schedules-sync
  - single-run
  - test
  - pre-hygiene
  - hygiene
  - report
  - close-reports
  - one-off
  - build
  - secrets
  - deploy
  - health-check

workflow:
  name: '$PIPELINE_NAME'
  rules:
    - if: $TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS
      variables:
        PIPELINE_NAME: 'Label community contributions'

    - if: $TRIAGE_GITLAB_FOSS_MIGRATE_ISSUES_TO_GITLAB
      variables:
        PIPELINE_NAME: 'Migrate gitlab-foss issues to gitlab'

    - if: $TRIAGE_UNTRIAGED_COMMUNITY_MERGE_REQUESTS_REPORT
      variables:
        PIPELINE_NAME: 'Generate untriaged community MRs report'

    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
      variables:
        PIPELINE_NAME: '$CI_MERGE_REQUEST_EVENT_TYPE MR pipeline'
    # For `master` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For dry-running a schedule with `dry-run:schedule`
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images/debian-bullseye-ruby-3.0:bundler-2.3
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile.lock
      prefix: "ruby-gems"
    paths:
      - vendor/ruby/

variables:
  BUNDLE_PATH__SYSTEM: 'false'
  BUNDLE_WITHOUT: "production:development"
  BUNDLE_INSTALL_FLAGS: "--jobs=$(nproc) --retry=3 --quiet"
  TRIAGE_WEB_IMAGE_PATH: "${CI_REGISTRY}/${CI_PROJECT_PATH}/web:${CI_COMMIT_REF_SLUG}"
  DOCKER_VERSION: "20.10.1"
  SIMPLECOV: "true"
  JUNIT_RESULT_FILE: "rspec/junit_rspec.xml"
  COVERAGE_FILE: "coverage/coverage.xml"

.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

include:
  - local: .gitlab/ci/*.yml
  - local: .gitlab/ci/generated/*.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/danger-review.yml'
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml

danger-review:
  rules:
    - if: '$CI_MERGE_REQUEST_LABELS =~ /one-off/'
      allow_failure: true
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  before_script: !reference [.ruby-before_script, before_script]
  variables:
    BUNDLE_WITHOUT: "production:development:test"
    BUNDLE_WITH: "danger"

brakeman-sast:
  needs: []
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'

secret_detection:
  needs: []
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'

gemnasium-dependency_scanning:
  needs: []
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
