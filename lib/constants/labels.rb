# frozen_string_literal: true

module Labels
  CI_COMPONENTS_LABEL = 'ci::components'
  COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'
  CONTRACTOR_CONTRIBUTION_LABEL = 'Contractor Contribution'
  LEADING_ORGANIZATION_LABEL = 'Leading Organization'
  FIRST_CONTRIBUTION_LABEL = '1st contribution'

  UX_LABEL = 'UX'
  FRONTEND_LABEL = 'frontend'
  BACKEND_LABEL = 'backend'

  FEATURE_FLAG_LABEL = 'feature flag'

  DOCUMENTATION_LABEL = 'documentation'
  DOCS_ONLY_LABEL = 'docs-only'
  TECHNICAL_WRITING_LABEL = 'Technical Writing'
  TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'

  MR_APPROVED_LABEL = 'pipeline:mr-approved'

  HACKATHON_LABEL = 'Hackathon'

  WORKFLOW_FAILED_CURRENT_VERSION_LABEL = 'workflow::failed current version'
  WORKFLOW_PROBLEM_VALIDATION_LABEL = 'workflow::problem validation'
  WORKFLOW_START_LABEL = 'workflow::start'
  WORKFLOW_TW_TEST_STATUS_LABEL = 'workflow::tw-test-status'
  WORKFLOW_VALIDATION_BACKLOG_LABEL = 'workflow::validation backlog'
  WORKFLOW_NEEDS_ISSUE_REVIEW_LABEL = 'workflow::needs issue review'

  ISSUE_VALIDATION_LABEL = 'issue::validation'
  NEW_WORKFLOW_VALIDATION = [
    WORKFLOW_FAILED_CURRENT_VERSION_LABEL,
    WORKFLOW_PROBLEM_VALIDATION_LABEL,
    WORKFLOW_START_LABEL,
    WORKFLOW_TW_TEST_STATUS_LABEL,
    WORKFLOW_VALIDATION_BACKLOG_LABEL,
    WORKFLOW_NEEDS_ISSUE_REVIEW_LABEL
  ].freeze

  WORKFLOW_REFINEMENT_LABEL = 'workflow::refinement'
  WORKFLOW_PLANNING_BREAKDOWN_LABEL = 'workflow::planning breakdown'
  WORKFLOW_DESIGN_LABEL = 'workflow::design'
  WORKFLOW_ISSUE_REVIEWED_LABEL = 'workflow::issue reviewed'
  WORKFLOW_OPEN_FOR_DISCUSSION_LABEL = 'workflow::open for discussion'
  WORKFLOW_READY_FOR_DESIGN_LABEL = 'workflow::ready for design'
  WORKFLOW_SCHEDULED_LABEL = 'workflow::scheduling'
  WORKFLOW_SOLUTION_VALIDATION_LABEL = 'workflow::solution validation'

  ISSUE_PLANNING_LABEL = 'issue::planning'
  NEW_WORKFLOW_PLANNING = [
    WORKFLOW_REFINEMENT_LABEL,
    WORKFLOW_PLANNING_BREAKDOWN_LABEL,
    WORKFLOW_DESIGN_LABEL,
    WORKFLOW_ISSUE_REVIEWED_LABEL,
    WORKFLOW_OPEN_FOR_DISCUSSION_LABEL,
    WORKFLOW_READY_FOR_DESIGN_LABEL,
    WORKFLOW_SCHEDULED_LABEL,
    WORKFLOW_SOLUTION_VALIDATION_LABEL
  ].freeze

  WORKFLOW_READY_FOR_DEVELOPMENT_LABEL = 'workflow::ready for development'
  WORKFLOW_IN_DEV_LABEL = 'workflow::in dev'
  WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'
  WORKFLOW_IN_REVIEW_LABEL = 'workflow::in review'
  WORKFLOW_FEATURE_FLAGGED_LABEL = 'workflow::feature-flagged'
  WORKFLOW_AWAITING_SECURITY_RELEASE_LABEL = 'workflow::awaiting security release'
  WORKFLOW_BLOCKED_LABEL = 'workflow::blocked'

  ISSUE_DEVELOPMENT_LABEL = 'issue::development'
  NEW_WORKFLOW_DEVELOPMENT = [
    WORKFLOW_READY_FOR_DEVELOPMENT_LABEL,
    WORKFLOW_IN_DEV_LABEL,
    WORKFLOW_READY_FOR_REVIEW_LABEL,
    WORKFLOW_IN_REVIEW_LABEL,
    WORKFLOW_FEATURE_FLAGGED_LABEL,
    WORKFLOW_AWAITING_SECURITY_RELEASE_LABEL,
    WORKFLOW_BLOCKED_LABEL
  ].freeze

  WORKFLOW_VERIFICATION = 'workflow::verification'
  WORKFLOW_COMPLETE = 'workflow::complete'

  WORKFLOW_STAGING_CANARY = 'workflow::staging-canary'
  WORKFLOW_STAGING = 'workflow::staging'
  WORKFLOW_STAGING_REF = 'workflow::staging-ref'
  WORKFLOW_CANARY = 'workflow::canary'
  WORKFLOW_PRODUCTION = 'workflow::production'
  WORKFLOW_ENVIRONMENTS = [
    WORKFLOW_STAGING_CANARY,
    WORKFLOW_STAGING,
    WORKFLOW_STAGING_REF,
    WORKFLOW_CANARY,
    WORKFLOW_PRODUCTION
  ].freeze
  WORKFLOW_DB_PRODUCTION = 'workflow::post-deploy-db-production'
  WORKFLOW_DB_STAGING = 'workflow::post-deploy-db-staging'
  WORKFLOW_VERSION_PROMOTED_LABEL = 'workflow::version promoted'

  ISSUE_COMPLETE_LABEL = 'issue::complete'
  NEW_WORKFLOW_COMPLETE = WORKFLOW_ENVIRONMENTS + [
    WORKFLOW_DB_PRODUCTION,
    WORKFLOW_DB_STAGING,
    WORKFLOW_VERIFICATION,
    WORKFLOW_COMPLETE,
    WORKFLOW_VERSION_PROMOTED_LABEL
  ].freeze

  SEEKING_COMMUNITY_CONTRIBUTIONS = 'Seeking community contributions'

  IDLE_LABEL = 'idle'
  STALE_LABEL = 'stale'

  AUTOMATION_AUTHOR_REMINDED_LABEL = 'automation:author-reminded'
  AUTOMATION_REVIEWERS_REMINDED_LABEL = 'automation:reviewers-reminded'

  FEDRAMP_VULNERABILITY_LABEL = 'FedRAMP::Vulnerability'
  VULNERABILITY_SLA_LABEL = 'Vulnerability SLA'
  VULNERABILITY_FIX_AVAILABLE_LABELS = [
    'Vulnerability::Vendor Package::Fix Available',
    'Vulnerability::Vendor Base Container::Fix Available'
  ].freeze

  INFRADEV_LABEL = 'infradev'

  TYPE_LABELS = [
    'type::feature',
    'type::maintenance',
    'type::bug'
  ].freeze

  BUG_VULNERABILITY_LABEL = 'bug::vulnerability'

  TYPE_IGNORE_LABEL = 'type::ignore'

  SPECIAL_ISSUE_LABELS = [
    'support request',
    'meta',
    'triage report'
  ].freeze

  # Govern:Threat Insights labels
  THREAT_INSIGHTS_GROUP_LABEL = 'group::threat insights'
  THREAT_INSIGHTS_TEAM_LABELS = [
    'threat insights::navy',
    'threat insights::tangerine'
  ].freeze

  SPAM_LABEL = 'Spam'

  MASTER_BROKEN_LABEL = 'master:broken'
  MASTER_FOSS_BROKEN_LABEL = 'master:foss-broken'
  PIPELINE_EXPEDITE_LABEL = 'pipeline:expedite'
  FLAKY_TEST_LABEL_PREFIX = 'flaky-test::'

  QUARANTINE_LABEL = 'quarantine'
  QUARANTINE_FLAKY_LABEL = 'quarantine::flaky'

  MASTER_BROKEN_ROOT_CAUSE_LABELS = {
    default: 'master-broken::undetermined',
    flaky_test: 'master-broken::flaky-test',
    dependency_upgrade: 'master-broken::dependency-upgrade',
    failed_to_pull_image: 'master-broken::failed-to-pull-image',
    gitlab_com_overloaded: 'master-broken::gitlab-com-overloaded',
    runner_disk_full: 'master-broken::runner-disk-full',
    infrastructure: 'master-broken::infrastructure',
    job_timeout: 'master-broken::job-timeout'
  }.freeze

  # Growth team labels
  GROWTH_TEAM_LABELS = [
    'section::growth',
    'Next Up'
  ].freeze

  # Quality
  QUALITY_LABEL = 'Quality'
  ENGINEERING_PRODUCTIVITY_LABEL = 'Engineering Productivity'

  # Database labels
  DATABASE_APPROVED_LABEL = 'database::approved'
  DATABASE_REVIEWED_LABEL = 'database::reviewed'

  # Govern:Compliance labels
  COMPLIANCE_GROUP_LABEL = 'group::compliance'
  VERIFIED_BY_AUTHOR = 'verified-by-author'

  # Groups that want their issues that are closed by a merge request to be
  # automatically tagged with a workflow::complete label
  WORKFLOW_AUTOMATION = {
    'group::gitaly' => WORKFLOW_COMPLETE,
    'group::distribution' => WORKFLOW_COMPLETE
  }.freeze
end
