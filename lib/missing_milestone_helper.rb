# frozen_string_literal: true

require 'date'

module MissingMilestoneHelper
  def set_milestone_for_merge_request(merge_request)
    milestone = milestone_for_merge_request(merge_request)
    return unless milestone

    %(/milestone %"#{milestone.title}")
  end

  private

  def milestone_for_merge_request(merge_request)
    return unless merge_request[:merged_at]

    VersionedMilestone.new(self).find_milestone_for_date(Date.parse(merge_request[:merged_at]))
  end
end
