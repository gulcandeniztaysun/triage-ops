# frozen_string_literal: true

require 'cgi'
require 'gitlab/triage/engine'
require_relative '../triage/triage'

class LazyHeatMap
  PRIORITY_LABELS = %w[priority::1 priority::2 priority::3 priority::4].freeze
  SEVERITY_LABELS = %w[severity::1 severity::2 severity::3 severity::4].freeze
  FLAKINESS_LABELS = %w[flakiness::1 flakiness::2 flakiness::3 flakiness::4].freeze
  QUARANTINE_LABELS = %w[quarantine].freeze
  WITHOUT_PRIORITY_STRING = 'No priority'
  WITHOUT_SEVERITY_STRING = 'No severity'
  WITHOUT_FLAKINESS_STRING = 'No flakiness status'
  NOT_QUARANTINED_STRING = 'No quarantined'
  QUARANTINED_TITLES = ['quarantine', NOT_QUARANTINED_STRING].freeze

  DIMENSIONS = {
    priority: {
      labels: PRIORITY_LABELS,
      none: WITHOUT_PRIORITY_STRING,
      titles: [*PRIORITY_LABELS, WITHOUT_PRIORITY_STRING]
    },
    severity: {
      labels: SEVERITY_LABELS,
      none: WITHOUT_SEVERITY_STRING,
      titles: [*SEVERITY_LABELS, WITHOUT_SEVERITY_STRING]
    },
    flakiness: {
      labels: FLAKINESS_LABELS,
      none: WITHOUT_FLAKINESS_STRING,
      titles: [*FLAKINESS_LABELS, WITHOUT_FLAKINESS_STRING]
    },
    quarantined: {
      labels: QUARANTINE_LABELS,
      none: NOT_QUARANTINED_STRING,
      titles: [*QUARANTINE_LABELS, NOT_QUARANTINED_STRING]
    }
  }.freeze

  InvalidDimensionError = Class.new(StandardError)

  def initialize(resources, policy_spec, network)
    @resources = resources
    @policy_spec = policy_spec
    @network = network
    @options = network.options
  end

  def to_s
    @to_s ||= generate_heat_map_table
  end

  def generate_heat_map_table(dimension_1:, dimension_2:)
    validate_dimensions!(dimension_1, dimension_2)

    heat_map = generate_heat_map(dimension_1, dimension_2)
    body = heat_map.each_key.map do |dimension_1_value|
      row = [header_string([dimension_1, dimension_1_value])] +
        DIMENSIONS.dig(dimension_2, :titles).map do |dimension_2_value|
          count = heat_map.dig(dimension_1_value, dimension_2_value)&.size || 0
          issues_link(count, issues_query_string([dimension_1, dimension_1_value], [dimension_2, dimension_2_value]))
        end

      "| #{row.join(' | ')} |"
    end.join("\n")

    header = "|| #{DIMENSIONS.dig(dimension_2, :titles).map { |dimension_2_value| header_string([dimension_2, dimension_2_value]) }.join(' | ')} |"
    separator = "|----|#{DIMENSIONS.dig(dimension_2, :titles).map { '----' }.join('|')}|"

    "#{header}\n#{separator}\n#{body}"
  end

  def validate_dimensions!(dimension_1, dimension_2)
    invalid_dimensions = [dimension_1, dimension_2].compact - DIMENSIONS.keys

    raise InvalidDimensionError, "Invalid dimension(s) given: #{invalid_dimensions.map(&:inspect).join(', ')}" if invalid_dimensions.any?
  end

  def generate_heat_map_table_stuck
    no_assignees      = @resources.select { |resource| resource[:assignees].empty? }
    no_milestone      = @resources.select { |resource| resource[:milestone].nil? }
    milestone_backlog = @resources.select { |resource| resource.dig(:milestone, :title) == 'Backlog' }
    milestone_expired = @resources.select { |resource| resource.dig(:milestone, :expired) }

    data = {
      'No assignees' => {
        issues: group_by_label(no_assignees, :severity),
        filter: { filter_key: 'assignee_id', filter_value: 'None' }
      },
      'No milestone' => {
        issues: group_by_label(no_milestone, :severity),
        filter: { filter_key: 'milestone_title', filter_value: 'None' }
      },
      'Milestone: Backlog' => {
        issues: group_by_label(milestone_backlog, :severity),
        filter: { filter_key: 'milestone_title', filter_value: 'Backlog' }
      },
      'Milestone in the past' => {
        issues: group_by_label(milestone_expired, :severity),
        filter: { filter_key: 'not[milestone_title]', filter_value: 'Upcoming' }
      }
    }

    body = data.map do |row_title, row_hash|
      row = [row_title] +
        row_hash[:issues].map do |severity, issues|
          count = issues&.size || 0
          issues_link(count, issues_query_string(nil, [:severity, severity], **row_hash[:filter]))
        end

      "| #{row.join(' | ')} |"
    end.join("\n")

    header     = "|| #{DIMENSIONS.dig(:severity, :titles).map { |s| header_string([:severity, s]) }.join(' | ')} |"
    separator  = "|----|#{DIMENSIONS.dig(:severity, :titles).map { '----' }.join('|')}|"

    "#{header}\n#{separator}\n#{body}"
  end

  private

  def generate_heat_map(dimension_1, dimension_2)
    group_by_label(@resources, dimension_1).transform_values do |with_same_dimension_1|
      group_by_label(with_same_dimension_1, dimension_2)
    end
  end

  def pad_and_sort(hash, labels)
    labels.each do |name|
      hash[name] ||= {}
    end

    hash.sort_by { |k, _| k[/\d/] || 'Z' }.to_h
  end

  def group_by_label(resources, dimension)
    details = DIMENSIONS.fetch(dimension)
    grouped_resources = resources.group_by { |resource| (resource[:labels] & details[:labels]).min || details[:none] }
    pad_and_sort(grouped_resources, details[:titles])
  end

  def issues_link(count, query_string, options = {})
    if count.zero?
      "-"
    else
      <<~MARKDOWN.chomp
        [#{count}](#{issues_base_url(options)}?#{query_string})
      MARKDOWN
    end
  end

  def issues_base_url(options = {})
    return if @resources.empty?

    # https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage/-/issues/255, we could use
    # `@policy_spec.dig(:limits, :project)` instead of passing options.
    source_url_for_resource(@resources.first, project_id: options[:project_id])
  end

  def base_issue_query(dimension_1_array, dimension_2_array)
    {
      'state' => issues_states,
      'label_name[]' => issues_label_names + with_dimensions_labels(dimension_1_array, dimension_2_array),
      'not[label_name][]' => issues_forbidden_label_names + without_dimensions_labels(dimension_1_array, dimension_2_array)
    }
  end

  def issues_query_string(dimension_1_array, dimension_2_array, filter_key: nil, filter_value: nil)
    query = base_issue_query(dimension_1_array, dimension_2_array)
    query[filter_key] = [filter_value] if filter_key && filter_value

    escape_issues_conditions(query)
  end

  def issues_states
    [@policy_spec.dig(:conditions, :state)].compact
  end

  def issues_label_names
    @policy_spec.dig(:conditions, :labels) || []
  end

  def issues_forbidden_label_names
    @policy_spec.dig(:conditions, :forbidden_labels) || []
  end

  def escape_issues_conditions(conditions)
    conditions.flat_map do |key, values|
      values.map { |v| "#{CGI.escape(key)}=#{CGI.escape(v)}" }
    end.join('&')
  end

  def header_string(dimension_array)
    dimension, dimension_value = dimension_array

    if dimension_value == DIMENSIONS.dig(dimension, :none)
      dimension_value
    else
      %(~"#{dimension_value}")
    end
  end

  def with_dimensions_labels(dimension_1_array, dimension_2_array)
    dimension_1, dimension_1_value = dimension_1_array
    dimension_2, dimension_2_value = dimension_2_array

    [
      *(dimension_1_value unless dimension_1_value == DIMENSIONS.dig(dimension_1, :none)),
      *(dimension_2_value unless dimension_2_value == DIMENSIONS.dig(dimension_2, :none))
    ]
  end

  def without_dimensions_labels(dimension_1_array, dimension_2_array)
    dimension_1, dimension_1_value = dimension_1_array
    dimension_2, dimension_2_value = dimension_2_array

    [
      *(DIMENSIONS.dig(dimension_1, :labels) if dimension_1_value == DIMENSIONS.dig(dimension_1, :none)),
      *(DIMENSIONS.dig(dimension_2, :labels) if dimension_2_value == DIMENSIONS.dig(dimension_2, :none))
    ]
  end

  def source_url_for_resource(resource, project_id: nil)
    context = Gitlab::Triage::Resource::Context.build(
      resource, network: @network)

    resource_type = context.class.name.demodulize.underscore.pluralize

    source_id = project_id || @options.source_id
    source =
      if project_id || @options.source == 'projects'
        context.__send__(:request_project, source_id)
      else
        context.__send__(:request_group, source_id)
      end

    "#{source[:web_url]}/-/#{resource_type}"
  end
end

module SummaryBuilderWithHeatMap
  private

  def description_resource
    super.merge(heat_map: LazyHeatMap.new(@resources, @policy_spec, @network))
  end
end
