# frozen_string_literal: true

require_relative '../../lib/google_sheets_helper'
require_relative '../triage'

module Triage
  class LeadingOrganizationDetector
    include Logging

    SHEET_NAME = 'Leading Organization Users'
    ONE_DAY = 24 * 60 * 60

    def leading_organization?(username)
      if google_service_account_credentials_path.empty?
        logger.warn('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH env variable not defined.')

        return
      end

      if sheet_id.empty?
        logger.warn('LEADING_ORGS_TRACKER_SHEET_ID env variable not defined.')

        return
      end

      leading_organization_usernames.include?(username&.downcase)
    end

    private

    def google_service_account_credentials_path
      @google_service_account_credentials_path ||= ENV.fetch('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS_PATH', '')
    end

    def sheet_id
      @sheet_id ||= ENV.fetch('LEADING_ORGS_TRACKER_SHEET_ID', '')
    end

    def leading_organization_usernames
      Triage.cache.get_or_set(:leading_organization_usernames, expires_in: ONE_DAY) do
        GoogleSheetsHelper.new(
          sheet_id,
          SHEET_NAME,
          google_service_account_credentials_path
        ).read.map { |row| row['username'].downcase }
      end
    end
  end
end
