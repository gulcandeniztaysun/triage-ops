# frozen_string_literal: true

require_relative '../../triage'

module Triage
  module PipelineFailure
    class PipelineIncidentFinder
      DESCRIPTION = 'DESCRIPTION'
      RETRY_MAX_LIMIT = 2

      attr_reader :incident_project_id, :pipeline_id

      def initialize(incident_project_id:, pipeline_id: nil)
        @incident_project_id = incident_project_id
        @pipeline_id = pipeline_id
      end

      def latest_incident
        @latest_incident ||= incidents.last
      end

      def incidents
        return @incidents if defined?(@incidents)

        @incidents = []
        search_params = pipeline_id ? { search: pipeline_id, in: DESCRIPTION } : {}
        retries = 0

        begin
          @incidents =
            Triage.api_client.issues(
              incident_project_id,
              search_params.merge({ order_by: 'created_at', sort: 'desc', per_page: 2 })
            )
        rescue Gitlab::Error::InternalServerError
          retries += 1
          retry if retries <= RETRY_MAX_LIMIT
        end

        @incidents
      end
    end
  end
end
