# frozen_string_literal: true

require_relative 'base'
require_relative '../failed_jobs'
require_relative '../triage_incident'
require_relative '../../../../lib/constants/labels'

module Triage
  module PipelineFailure
    module Config
      class ApprovedMr < Base
        INCIDENT_PROJECT_ID = '54990218' # gitlab-org/quality/engineering-productivity/approved-mr-pipeline-incidents
        INCIDENT_LABELS = [Labels::ENGINEERING_PRODUCTIVITY_LABEL].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN.freeze
          #{DEFAULT_INCIDENT_SUMMARY_TABLE}

          %<attribution_body>s
        MARKDOWN

        SLACK_CHANNEL = 'mr-blocked-by-master-broken'

        DEFAULT_DUPLICATE_COMMAND_BODY = <<~MARKDOWN.chomp
          Likely related to `master` incident %<duplicate_incident_url>s. Please reopen it if you don't think this is a duplicate.

          /copy_metadata %<duplicate_incident_url>s
          /duplicate %<duplicate_incident_url>s
        MARKDOWN

        def self.failed_jobs(event)
          Triage::PipelineFailure::FailedJobs.new(event).execute
        end

        def self.triager(event)
          TriageIncident.new(
            event: event,
            config: new(event),
            ci_jobs: failed_jobs(event).map do |job|
              Triage::CiJob.new(
                instance: event.instance,
                project_id: event.project_id,
                job_id: job.id,
                name: job.name,
                web_url: job.web_url
              )
            end
          )
        end

        def self.match?(event)
          event.on_instance?(:com) &&
            event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            event.merge_request_pipeline? &&
            event.merge_request_approved? &&
            event.status == 'failed' &&
            event.source_job_id.nil? &&
            triager(event).duplicate?
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def canonical_incident_project_id
          Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          INCIDENT_LABELS
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end

        def duplicate_command_body
          DEFAULT_DUPLICATE_COMMAND_BODY
        end
      end
    end
  end
end
