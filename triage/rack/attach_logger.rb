# frozen_string_literal: true

require 'rack'

module Triage
  module Rack
    # Inspired by https://github.com/eropple/rack-ougai/blob/master/lib/rack/ougai/constant_logger.rb
    AttachLogger = Struct.new(:app, :logger) do
      def call(env)
        env[::Rack::RACK_LOGGER] = logger
        app.call(env)
      end
    end
  end
end
