# frozen_string_literal: true

require_relative 'database_processor'

module Triage
  class DatabaseReviewExperienceFeedback < DatabaseProcessor
    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      database_reviewed_or_approved? &&
        unique_comment.no_previous_comment? &&
        not_spam? &&
        !event.automation_author?
    end

    def process
      post_database_review_experience_comment
    end

    def documentation
      <<~TEXT
        This processor asks for feedback about the database review experience.
      TEXT
    end

    private

    def post_database_review_experience_comment
      add_comment(message.strip, append_source_link: true)
    end

    def message
      comment = <<~MARKDOWN.chomp
        Hello @#{event.resource_author.username} :wave:

        The database team is looking for ways to improve the database review
        process and we would love your help!

        If you'd be open to someone on the database team reaching out to you
        for a chat, or if you'd like to leave some feedback asynchronously,
        just post a reply to this comment mentioning:

        ```
        @gitlab-org/database-team
        ```

        And someone will be by shortly!

        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end
  end
end
