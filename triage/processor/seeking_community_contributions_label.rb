# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class SeekingCommunityContributionsLabel < Processor
    DESCRIPTION_IMPLEMENTATION_GUIDE_PATTERN = /# implementation/i
    SEEKING_COMMUNITY_CONTRIBUTIONS_LABEL = 'Seeking community contributions'

    react_to 'issue.*'

    def applicable?
      # Repeat the comment every time the label is added
      event.from_gitlab_org? && seeking_community_contribution_label_added? && issue_missing_implementation_guide?
    end

    def process
      post_implementation_guide_comment
    end

    def documentation
      <<~TEXT
        This processor reacts to the `Seeking community contributions` label being added to an issue,
        and comments to suggest adding an "Implementation Guide" section if one does not already exist in the issue description.
        The intent is to help ensure that these issues are truly prepared for community contributions
        and can be easily executed by anyone who picks it up.
      TEXT
    end

    private

    def implementation_guide_comment
      comment = <<~MARKDOWN.chomp
        @#{event.event_actor_username} thanks for adding the ~"#{SEEKING_COMMUNITY_CONTRIBUTIONS_LABEL}" label!

        This issue's description does not seem to have a section for "Implementation Guide".
        Please consider adding one, because it makes a [big difference for contributors](https://about.gitlab.com/handbook/engineering/development/dev/create/ide/community-contributions/#treat-wider-community-as-primary-audience).
        This section can be brief but must have clear technical guidance, like:

        - Hints on lines of code which may need changing
        - Hints on similar code/patterns that can be leveraged
        - Suggestions for test coverage
        - Ideas for breaking up the merge requests into iterative chunks
        - Links to documentation (within GitLab or external) about implementation or testing guidelines, especially when working with third-party libraries

        Need help? Reach out to the [Contributor Success](https://about.gitlab.com/handbook/marketing/developer-relations/contributor-success/) team in #contributor-success, or in [Discord](https://discord.gg/gitlab).
      MARKDOWN
    end

    def issue_description
      event.description
    end

    def issue_missing_implementation_guide?
      !issue_description.match?(DESCRIPTION_IMPLEMENTATION_GUIDE_PATTERN)
    end

    def seeking_community_contribution_label_added?
      event.added_label_names.include?(SEEKING_COMMUNITY_CONTRIBUTIONS_LABEL)
    end

    def post_implementation_guide_comment
      add_comment(implementation_guide_comment.strip, append_source_link: true)
    end
  end
end
