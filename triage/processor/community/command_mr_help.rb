# frozen_string_literal: true

require 'digest'

require_relative 'community_processor'

require_relative '../../../lib/team_member_helper'
require_relative '../../../lib/discord_messenger'

module Triage
  class CommandMrHelp < CommunityProcessor
    include RateLimit
    include TeamMemberHelper

    react_to 'merge_request.note'
    define_command name: 'help'

    def initialize(event)
      super(event)

      @discord_messenger = DiscordMessenger.new(ENV.fetch('DISCORD_WEBHOOK_PATH_COMMUNITY_MR_HELP', nil))
    end

    def applicable?
      valid_command?
    end

    def process
      comment = <<~MARKDOWN.chomp.concat("\n")
        Hey there #{coach}, could you please help @#{event.event_actor_username} out?
        /assign_reviewer #{coach}
      MARKDOWN
      add_comment(comment, append_source_link: true)

      post_discord_message if event.project_public?
    end

    def documentation
      <<~TEXT
        This processor supports the "@gitlab-bot help" triage operation command to assign a merge request coach as code reviewer and sends a message to Discord.
      TEXT
    end

    private

    attr_reader :discord_messenger

    def coach
      @coach ||= select_random_merge_request_coach
    end

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("help-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      event.by_team_member? ? 100 : 1
    end

    def rate_limit_period
      3600 # 1 hour
    end

    def post_discord_message
      message = <<~TEXT
        `#{event.resource_author.username}` is looking for help on a merge request:
        - URL: #{event.url}
        - Title: `#{event.title}`
        - Project: `#{event.project_path_with_namespace}`
        - Labels: #{format_current_labels_list}
      TEXT

      discord_messenger.send_message(message)
    end
  end
end
