# frozen_string_literal: true

require 'digest'
require 'slack-messenger'

require_relative 'community_processor'

module Triage
  class CommandMrFeedback < CommunityProcessor
    include RateLimit

    SLACK_CHANNEL = '#mr-feedback'
    SLACK_ICON = ':robot_face:'
    SLACK_MESSAGE_TEMPLATE = <<~MESSAGE
      Hola MR coaches, `@%<username>s` has left feedback about their experience in %<comment_url>s.
    MESSAGE

    react_to 'merge_request.note'
    define_command name: 'feedback'

    def initialize(event, messenger: slack_messenger)
      super(event)
      @messenger = messenger
    end

    def applicable?
      valid_command?
    end

    def process
      post_mr_feedback_request
      post_reaction_on_feedback
    end

    def slack_options
      {
        channel: SLACK_CHANNEL,
        username: GITLAB_BOT,
        icon_emoji: SLACK_ICON
      }
    end

    def documentation
      <<~TEXT
        This processor pings the #{SLACK_CHANNEL} channel when community contributors provided code review feedback.

        In addition, it reacts with a checkmark to the user giving the feedback.
      TEXT
    end

    private

    attr_reader :messenger

    def cache_key
      @cache_key ||= Digest::MD5.hexdigest("feedback-commands-sent-#{event.event_actor_id}-#{event.noteable_path}")
    end

    def rate_limit_count
      1
    end

    def rate_limit_period
      86400 # 1 day
    end

    def post_mr_feedback_request
      message = format(SLACK_MESSAGE_TEMPLATE, comment_url: event.url, username: event.event_actor_username)
      note = {
        text: get_note_text(event)
      }

      messenger.ping(text: message, attachments: [note])
    end

    def post_reaction_on_feedback
      add_emoji_award("white_check_mark")
    rescue Gitlab::Error::NotFound
      # if emoji is already given, it will return a 404 status code.
      # Gitlab::Error::NotFound: Server responded with code 404,
      # message: 404 Award Emoji Name has already been taken Not Found.
      #
      # We don't need to do anything in that case.
    end

    def slack_messenger
      Slack::Messenger.new(ENV.fetch('SLACK_WEBHOOK_URL', nil), slack_options)
    end

    def get_note_text(event)
      event.new_comment.sub("#{GITLAB_BOT} feedback ", "")
    end
  end
end
