# frozen_string_literal: true

require_relative 'customer_contribution_notifier'

module Triage
  class CustomerContributionCreatedNotifier < CustomerContributionNotifier
    react_to 'merge_request.open'

    def customer_contribution_message_template
      msg = <<~MESSAGE
      > New Customer MR Created -
      MESSAGE

      msg + super
    end

    def slack_channel
      '#contribution-efficiency'
    end

    def documentation
      <<~TEXT
        This processor pings the #contribution-efficiency channel when a customer creates a community contribution merge request.
      TEXT
    end
  end
end
